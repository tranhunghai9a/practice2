import 'package:flutter/cupertino.dart';

mixin CommonValidation {
  String? validateEmail(String? value) {
    if (value!.isEmpty) {
      return 'Please fill the blank';
    }
    if (!value.contains('.')) {
      return 'It must contain dot';
    }
    if (!value.contains('@')) {
      return 'It must contain @';
    }

    return null;
  }

  String? validateLastName(String? value) {
    if (value!.isEmpty) {
      return 'Please fill the blank';
    }
    if (!value.contains(RegExp(r'^(?=.*?[A-Z])'))) {
      return 'Please Uppercase your own Name';
    }
    if (value.contains(RegExp(r'^(?=.*?[!@#$%^&*~])'))) {
      return 'Your real name does not really have special character, does it? ';
    }
    if (value.contains(RegExp(r'^(?=.*?[0-9])'))) {
      return 'Your real name does not really have numbers, does it? ';
    }

    return null;
  }

  String? validateFirstName(String? value) {
    if (value!.isEmpty) {
      return 'Please fill the blank';
    }
    if (!value.contains(RegExp(r'^(?=.*?[A-Z])'))) {
      return 'Please Uppercase your own Name';
    }
    if (value.contains(RegExp(r'^(?=.*?[!@#$%^&*~])'))) {
      return 'Your real name does not really have special character, does it? ';
    }
    if (value.contains(RegExp(r'^(?=.*?[0-9])'))) {
      return 'Your real name does not really have numbers, does it? ';
    }

    return null;
  }

  String? validateYear(String? value) {
    if (value!.isEmpty) {
      return 'Please fill the blank';
    }
    if (value.length != 4) {
      return '4 digits';
    }
    if (value.contains(RegExp(r'^(?=.*?[a-z])'))) {
      return 'numbers only';
    }
    if (value.contains(RegExp(r'^(?=.*?[A-Z])'))) {
      return 'numbers only';
    }
    if (value.contains(RegExp(r'^(?=.*?[!@#$%^&*~])'))) {
      return 'numbers only';
    }

    return null;
  }
}