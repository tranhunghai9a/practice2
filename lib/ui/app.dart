import 'package:flutter/material.dart';
import '../validation/mixins_validation.dart';
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Form",
      home: Scaffold(
        appBar: AppBar(title: Text('Information of user')),
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String lastName;
  late String firstName;
  late String year;



  @override
  Widget build(BuildContext context) {
    return Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              fieldEmailAddress(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              fieldLastname(),
              Container(margin: EdgeInsets.only(top: 40.0),),
              fieldFirstname(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              fieldYear(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              loginButton()
            ],
          ),
        )

    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email address'
      ),
      validator: validateEmail,
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }



  Widget fieldLastname() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Last Name'
      ),
      validator: validateLastName,
      onSaved: (value) {
        lastName = value as String;
      },
    );
  }

  Widget fieldFirstname() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'First Name'
      ),
      validator: validateFirstName,
      onSaved: (value) {
        firstName = value as String;
      },
    );
  }

  Widget fieldYear() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Birth year'
      ),
      validator: validateYear,
      onSaved: (value) {
        year = value as String;
      },
    );
  }


  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('Email: $emailAddress, Last Name: $lastName, First Name: $firstName, Birth year: $year');
          }
        },
        child: Text('sign up')
    );
  }

}